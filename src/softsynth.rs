use crate::{Adjust, Command, Mode, MODE, SOFT_FD};
use espeak_sys::espeak_PARAMETER;
use nix::errno::Errno;
use nix::fcntl;
use nix::fcntl::OFlag;
use nix::sys::select;
use nix::sys::stat;
use nix::sys::time::TimeSpec;
use nix::unistd;
use std::error::Error;
use std::os::unix::io::RawFd;
use std::sync::atomic::AtomicBool;
use std::sync::atomic::Ordering;
use std::sync::mpsc;
use std::sync::mpsc::SendError;
use std::sync::Arc;
use std::sync::Condvar;
use std::sync::Mutex;

pub fn thread(
    mode: Mode,
    run: Arc<AtomicBool>,
    tx: mpsc::Sender<Command>,
    lock_condition: Arc<(Mutex<bool>, Condvar)>,
) {
    let (lock, condition) = &*lock_condition;
    let soft_fd = match open_softsynth() {
        Ok(soft_fd) => soft_fd,
        Err(error) => {
            println!("softsynth open {error}");
            return;
        }
    };
    // Set it for use by SynthCallback.
    unsafe { SOFT_FD = soft_fd };
    let timeout = TimeSpec::new(1, 0);

    while run.load(Ordering::Relaxed) {
        let mut fd_set = select::FdSet::new();
        fd_set.insert(soft_fd);
        if let Err(error) = select::pselect(soft_fd + 1, &mut fd_set, None, None, &timeout, None) {
            println!("softsynth select {error}");
            break;
        }
        if !fd_set.contains(soft_fd) {
            continue;
        }

        const MAX_BUFFER_SIZE: usize = 16 * 1024;
        let mut buffer: [u8; MAX_BUFFER_SIZE] = [0; MAX_BUFFER_SIZE];
        let bytes_read = match unistd::read(soft_fd, &mut buffer) {
            Ok(bytes_read) => bytes_read,
            Err(error) => {
                println!("softsynth read {error}");
                continue;
            }
        };
        let mut buffer = buffer[..bytes_read].to_vec();
        buffer.push(0);
        let buffer = match String::from_utf8(buffer) {
            Ok(buffer) => buffer,
            Err(error) => {
                println!("softsynth parse {error}");
                continue;
            }
        };

        let buffer = match buffer.rsplit_once('\u{18}') {
            // Flush and start at last synthFlushChar.
            Some((_, chunk)) => {
                let mut flush = lock.lock().unwrap();
                *flush = true;
                while *flush && run.load(Ordering::Relaxed) {
                    flush = condition.wait(flush).unwrap();
                }
                chunk
            }
            None => &buffer,
        };
        match mode {
            Mode::Speakup => {
                // The read end has been dropped.
                if parse_buffer(buffer, &tx).is_err() {
                    break;
                }
            }
            Mode::Acsint => {
                println!("Acsint mode not implemented");
                break;
            }
        }
    }
    if let Err(error) = unistd::close(soft_fd) {
        println!("softsynth close {error}");
    };
}

fn parse_buffer(buffer: &str, tx: &mpsc::Sender<Command>) -> Result<(), SendError<Command>> {
    let mut start = 0;
    let mut command = false;
    for (i, c) in buffer.char_indices() {
        if !command {
            if !('\u{0}'..' ').contains(&c) || c == '\n' {
                continue;
            }

            if start < i {
                // Send buffer text snippet.
                let text = buffer[start..i].to_string();
                tx.send(Command::Speak(text))?
            }

            if c != '\u{1}' {
                start = i;
            } else {
                start = i + 1;
            }
            command = true;
            continue;
        }

        if c.is_ascii_digit() || c == '+' || c == '-' {
            continue;
        }
        if let Some(command) = parse_command(&buffer[start..i], c) {
            tx.send(command)?
        }
        start = i + 1;
        command = false;
    }
    Ok(())
}

fn parse_command(value: &str, command: char) -> Option<Command> {
    if command == 'P' {
        if !value.is_empty() {
            println!("parsed pause command with \"{value}\" value");
        }
        return Some(Command::Pause);
    }

    let mut chars = value.chars();
    let adjust = match chars.next() {
        Some('+') => Adjust::Increment,
        Some('-') => Adjust::Decrement,
        Some(_) => Adjust::Set,
        None => {
            println!("Commands without value already handled.");
            return None;
        }
    };
    let value = match adjust {
        Adjust::Set => value,
        _ => chars.as_str(),
    };
    let value: i32 = match value.parse() {
        Err(error) => {
            println!("{:?} {command} {error}", value.as_bytes());
            return None;
        }
        Ok(value) => value,
    };

    use espeak_PARAMETER::*;
    use Command::*;
    let command = match command {
        'b' => Set(espeakPUNCTUATION, value, adjust),
        'f' => Set(espeakRANGE, value, adjust),
        'i' => Mark(value),
        'p' => Set(espeakPITCH, value, adjust),
        'r' => Set(espeakRANGE, value, adjust),
        's' => Set(espeakRATE, value, adjust),
        'v' => Set(espeakVOLUME, value, adjust),
        _ => {
            println!("unknown command \"{command}\" with value \"{value}\"");
            return None;
        }
    };
    Some(command)
}

fn open_softsynth() -> Result<RawFd, Box<dyn Error>> {
    let path = "/dev/softsynthu";
    let path2 = "/dev/softsynth";
    let flags = OFlag::O_RDWR | OFlag::O_NONBLOCK;
    let permission = stat::Mode::empty();
    let soft_fd = match unsafe { MODE } {
        Mode::Acsint => libc::STDIN_FILENO,
        Mode::Speakup => match fcntl::open(path, flags, permission) {
            Err(Errno::ENOENT) => {
                println!("/dev/softsynthu not found, using /dev/softsynth");
                fcntl::open(path2, flags, permission)?
            }
            result => result?,
        },
    };
    Ok(soft_fd)
}
