use crate::{Adjust, Command, Mode, MODE, SOFT_FD};
use alsa::mixer::{MilliBel, Mixer, Selem};
use espeak_AUDIO_OUTPUT::AUDIO_OUTPUT_PLAYBACK;
use espeak_PARAMETER::*;
use espeak_POSITION_TYPE::POS_CHARACTER;
use espeak_sys::espeak_ERROR::*;
use espeak_sys::espeak_EVENT_TYPE::*;
use espeak_sys::*;
use nix::unistd;
use std::error::Error;
use std::ptr;
use std::sync::atomic::AtomicBool;
use std::sync::atomic::Ordering;
use std::sync::mpsc;
use std::sync::mpsc::TryRecvError;
use std::sync::Arc;
use std::sync::Condvar;
use std::sync::Mutex;

pub fn thread(
    voice: String,
    run: Arc<AtomicBool>,
    rx: mpsc::Receiver<Command>,
    lock_condition: Arc<(Mutex<bool>, Condvar)>,
) {
    let (lock, condition) = &*lock_condition;
    // synth starts paused so espeak will initialize on first command.
    let mut synth = Synth {
        voice,
        ..Default::default()
    };
    while run.load(Ordering::Relaxed) {
        let mut flush = lock.lock().unwrap();
        if *flush {
            unsafe {
                espeak_sys::espeak_Cancel();
            }
            // Empty out the pipe.
            while rx.try_recv().is_ok() {}
            *flush = false;
            condition.notify_one();
        }

        match rx.try_recv() {
            Ok(command) => synth.execute_command(command),
            Err(TryRecvError::Disconnected) => break,
            _ => (),
        }
    }
    synth.execute_command(Command::Pause);
}

struct Synth {
    alsa_volume: bool,
    pitch: i32,
    punctuation: i32,
    range: i32,
    rate: i32,
    volume: i32,
    voice: String,
    paused: bool,
}

impl Default for Synth {
    fn default() -> Self {
        Synth {
            alsa_volume: true,
            pitch: 5,
            punctuation: 0,
            range: 5,
            rate: 2,
            volume: 5,
            voice: String::new(),
            // execute_command => unpause => initialize.
            paused: true,
        }
    }
}

impl Synth {
    fn initialize(&mut self) {
        // Should return EE_INTERNAL_ERROR instead of -1.
        if 0 > unsafe { espeak_Initialize(AUDIO_OUTPUT_PLAYBACK, 0, ptr::null(), 0) } {
            panic!("Unable to initialize espeak.");
        }

        unsafe { espeak_SetSynthCallback(callback) };
        self.set_voice(self.voice.clone());
        self.set_parameter(espeakPITCH, self.pitch, Adjust::Set);
        self.set_parameter(espeakPUNCTUATION, self.punctuation, Adjust::Set);
        self.set_parameter(espeakRANGE, self.range, Adjust::Set);
        self.set_parameter(espeakRATE, self.rate, Adjust::Set);
        self.set_parameter(espeakVOLUME, self.volume, Adjust::Set);
        unsafe { espeak_SetParameter(espeakCAPITALS, 0, 0) };
    }

    fn unpause(&mut self) {
        if !self.paused {
            return;
        }
        self.initialize();
        self.paused = false;
    }

    fn pause(&mut self) {
        if self.paused {
            return;
        }
        unsafe {
            espeak_Cancel();
            espeak_Terminate();
        };
        self.paused = true;
    }

    fn execute_command(&mut self, command: Command) {
        if let Command::Pause = command {
            self.pause();
            return;
        }
        self.unpause();

        let error = match command {
            Command::Mark(mark) => {
                let mark = format!("<mark name=\"{mark}\"/>");
                unsafe {
                    espeak_Synth(
                        mark.as_ptr() as *const _,
                        mark.len(),
                        0,
                        POS_CHARACTER,
                        0,
                        espeakSSML,
                        ptr::null_mut(),
                        ptr::null_mut(),
                    )
                }
            }
            Command::Set(parameter, value, adjust) => self.set_parameter(parameter, value, adjust),
            // Original code only returns EE_OK.
            Command::SetVoice(_voice) => panic!("Tried setting voice."),
            Command::Speak(text) => self.speak(text),
            Command::Pause => panic!("Already handled pause."),
        };

        match error {
            EE_OK => (),
            EE_BUFFER_FULL => {
                println!("EE_BUFFER_FULL error");
                let timeout = std::time::Duration::new(1, 0);
                std::thread::sleep(timeout);
                // Espeak consumed command, we cannot execute again.
            },
            _ => println!("other espeak error"),
        }
    }

    fn set_parameter(
        &mut self,
        parameter: espeak_PARAMETER,
        value: i32,
        adj: Adjust,
    ) -> espeak_ERROR {
        let self_value = match parameter {
            espeakPITCH => &mut self.pitch,
            espeakPUNCTUATION => &mut self.punctuation,
            espeakRANGE => &mut self.range,
            espeakRATE => &mut self.rate,
            espeakVOLUME => &mut self.volume,
            _ => panic!(),
        };

        let value = match adj {
            Adjust::Set => value,
            Adjust::Decrement => *self_value - value,
            Adjust::Increment => *self_value + value,
        };

        let parameter_value = match parameter {
            espeakPITCH => value * 11,
            espeakPUNCTUATION => value,
            espeakRANGE => value * 11,
            espeakRATE => value * 41 + 80,
            espeakVOLUME => (value + 1) * 22,
            _ => panic!(),
        };

        let set_volume = matches!(parameter, espeakVOLUME);

        let rc = unsafe { espeak_SetParameter(parameter, parameter_value, 0) };
        if let espeak_ERROR::EE_OK = rc {
            *self_value = value;
            if set_volume && self.alsa_volume {
                if let Err(error) = set_alsa_volume(value) {
                    println!("set volume {error}");
                };
            };
        }
        rc
    }

    fn set_voice(&mut self, voice: String) -> espeak_ERROR {
        // empty voice causes segfault
        if voice.is_empty() {
            return espeak_ERROR::EE_OK;
        }

        self.voice = voice;
        let rc = unsafe { espeak_SetVoiceByName(self.voice.as_bytes().as_ptr() as *const _) };
        if let espeak_ERROR::EE_OK = rc {
            return rc;
        }
        let mut espeak_voice: espeak_VOICE = unsafe { std::mem::zeroed() };
        espeak_voice.languages = self.voice.as_bytes().as_ptr() as *const _;
        unsafe { espeak_SetVoiceByProperties(&mut espeak_voice) }
    }

    fn speak(&self, text: String) -> espeak_ERROR {
        if let Mode::Speakup = unsafe { MODE } {
            let mut chars = text.chars();
            if let (Some(c), None) = (chars.next(), chars.next()) {
                let text = if c == ' ' {
                    "<say-as interpret-as=\"tts:char\">&#32;</say-as>"
                } else {
                    "<say-as interpret-as=\"characters\">{c}</say-as>"
                };
                return unsafe {
                    espeak_Synth(
                        text.as_ptr() as *const _,
                        text.len(),
                        0,
                        POS_CHARACTER,
                        0,
                        espeakSSML,
                        ptr::null_mut(),
                        ptr::null_mut(),
                    )
                };
            }
        }
        let mode = match unsafe { MODE } {
            Mode::Acsint => espeakSSML,
            Mode::Speakup => 0,
        };
        unsafe {
            espeak_Synth(
                text.as_ptr() as *const _,
                text.len(),
                0,
                POS_CHARACTER,
                0,
                mode,
                ptr::null_mut(),
                ptr::null_mut(),
            )
        }
    }
}

fn set_alsa_volume(volume: i32) -> Result<(), Box<dyn Error>> {
    let mixer = Mixer::new("default", false)?;
    let volume = (volume + 1) * 5 + 50;
    for selem in mixer.iter().map(Selem::new).map(Option::unwrap) {
        // Enum snd_mixer_elem_type_t has two members with the same value.
        // should always be false: if (snd_mixer_elem_get_type(e) != SND_MIXER_ELEM_SIMPLE).
        if selem.is_enumerated() {
            continue;
        }
        if selem.has_playback_switch() {
            selem.set_playback_switch_all(1).unwrap();
        }
        if selem.has_playback_volume() {
            continue;
        }
        match selem.get_playback_db_range() {
            // Assume error if min and max are zero.
            (MilliBel(0), MilliBel(0)) => {
                println!("Failed to get dB range.");
                match selem.get_playback_volume_range() {
                    (0, 0) => println!("Failed to get volume range."),
                    (min, max) => {
                        let set = min + i64::from(volume) * (max - min) / 100;
                        selem.set_playback_volume_all(set)?;
                    }
                }
            }
            (MilliBel(min), MilliBel(max)) => {
                // 24dB amplitude is too small for using a logscale
                let set = if max - min < 2400 {
                    min + i64::from(volume) * (max - min) / 100
                } else {
                    let mut volume = volume as f32 / 100.0;
                    const SND_CTL_TLV_DB_GAIN_MUTE: i64 = -9999999;
                    if min != SND_CTL_TLV_DB_GAIN_MUTE {
                        let min = 10.0_f32.powf((min - max) as f32 / 6000.0);
                        volume = volume * (1.0 - min) + min;
                    }
                    (6000.0 * volume.log10()) as i64 + max
                };
                // Original used round to nearest.
                selem.set_playback_db_all(MilliBel(set), alsa::Round::Floor)?;
            }
        }
    }
    Ok(())
}

#[no_mangle]
extern "C" fn callback(_wav: *mut i16, _samples: i32, mut events: *mut espeak_EVENT) -> i32 {
    loop {
        let event = unsafe { &*events };
        match event.event_type {
            espeakEVENT_LIST_TERMINATED => return 0,
            espeakEVENT_MARK => {
                // event.id union should be stored as name in UTF-8.
                let mark: i32 = std::str::from_utf8(&event.id.to_ne_bytes())
                    .expect("name is UTF-8")
                    .parse()
                    .expect("name is an integer");
                if !(0..=255).contains(&mark) {
                    continue;
                }
                softsynth_reportindex(mark);
            }
            _ => (),
        }
        events = events.wrapping_offset(1);
    }
}

fn softsynth_reportindex(index: i32) {
    match unsafe { MODE } {
        Mode::Acsint => {
            let index: u8 = index.try_into().expect("between 0 and 255");
            println!("{}", index as char);
        }
        Mode::Speakup => {
            if let Err(error) = unistd::write(unsafe { SOFT_FD }, format!("{index}").as_bytes()) {
                println!("callback write {error}");
                panic!();
            }
        }
    }
}
