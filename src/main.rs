pub mod espeak;
pub mod softsynth;

use clap::{ArgEnum, Parser};
use espeak_sys::espeak_PARAMETER;
use nix::errno::Errno;
use nix::fcntl;
use nix::fcntl::FlockArg;
use nix::fcntl::OFlag;
use nix::sys::signal;
use nix::sys::stat;
use nix::unistd;
use nix::unistd::ForkResult;
use nix::unistd::Pid;
use nix::unistd::Whence;
use signal_hook::iterator::Signals;
use std::error::Error;
use std::os::unix::io::RawFd;
use std::path::Path;
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::{mpsc, Arc, Condvar, Mutex};
use std::thread;

pub enum Command {
    Mark(i32),
    Set(espeak_PARAMETER, i32, Adjust),
    SetVoice(String),
    Speak(String),
    Pause,
}

pub enum Adjust {
    Decrement,
    Set,
    Increment,
}

/// Speakup soft_synth using the espeak-ng speech synthesizer.
#[derive(Clone, Parser)]
#[clap(author, version, about, long_about = None)]
struct Args {
    /// Set path for pid file.
    #[clap(short, long, value_parser, default_value = "/var/run/espeakup.pid")]
    pid_path: String,

    /// Set voice.
    #[clap(short, long, value_parser, default_value = "")]
    voice: String,

    /// Drive the ALSA volume.
    #[clap(short, long, value_parser, default_value_t = true)]
    alsa_volume: bool,

    /// What mode to run espeakup-rs in.
    #[clap(short, long, value_parser, default_value_t = Mode::Speakup, arg_enum)]
    mode: Mode,

    /// Debug mode (stay in the foreground).
    #[clap(short, long, value_parser, default_value_t = false)]
    debug: bool,
}

static mut MODE: Mode = Mode::Speakup;
static mut SOFT_FD: RawFd = 0;

#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, ArgEnum)]
pub enum Mode {
    Speakup,
    Acsint,
}

fn start_daemon() -> Result<RawFd, Box<dyn Error>> {
    let (rx, tx) = unistd::pipe()?;
    let mut c: [u8; 1] = [1];
    let exit = move || {
        if let Err(error) = unistd::write(tx, &c) {
            println!("pipe write {error}");
        };
        unsafe { libc::_exit(1) };
    };
    // Wait for write from (grand)child.
    if let ForkResult::Parent { .. } = unsafe { unistd::fork()? } {
        if let Err(error) = unistd::read(rx, &mut c) {
            println!("pipe read {error}");
            unsafe { libc::exit(1) }
        }
        unsafe { libc::exit(c[0].into()) }
    }
    // Spawn grandchild.
    if let Err(error) = unistd::setsid() {
        println!("setsid {error}");
        exit();
    }
    match unsafe { unistd::fork() } {
        Err(error) => {
            println!("fork {error}");
            exit();
        }
        Ok(ForkResult::Parent { .. }) => unsafe { libc::_exit(0) },
        Ok(ForkResult::Child) => (),
    }
    // Return and continue running as grandchild.
    if let Err(error) = unistd::chdir("/") {
        println!("chdir {error}");
        exit();
    }
    Ok(tx)
}

fn is_running(pid_file: &Path) -> Result<bool, Box<dyn Error>> {
    use stat::Mode;
    let permission = Mode::S_IRUSR
        | Mode::S_IWUSR
        | Mode::S_IRGRP
        | Mode::S_IWGRP
        | Mode::S_IROTH
        | Mode::S_IWOTH;
    let pid_file = fcntl::open(pid_file, OFlag::O_RDWR | OFlag::O_CREAT, permission)?;
    let running = check(pid_file)?;
    if !running {
        // Write pid to file because it is open anyway.
        unistd::ftruncate(pid_file, 0)?;
        unistd::lseek(pid_file, 0, Whence::SeekSet)?;
        let pid = format!("{}", unistd::getpid());
        unistd::write(pid_file, pid.as_bytes())?;
    }
    unistd::close(pid_file)?;
    Ok(running)
}

fn check(pid_file: RawFd) -> Result<bool, Box<dyn Error>> {
    fcntl::flock(pid_file, FlockArg::LockExclusive)?;
    let mut pid: [u8; 15] = [0; 15];
    let bytes_read = unistd::read(pid_file, &mut pid)?;
    // .pid file was just opened and created or was empty.
    if bytes_read == 0 {
        return Ok(false);
    };
    let pid = pid[..bytes_read].to_vec();
    let pid = String::from_utf8(pid)?.parse()?;
    match signal::kill(Pid::from_raw(pid), None) {
        Err(error) => {
            println!("kill error {error}");
            // Whether the process was found.
            Ok(matches!(error, Errno::ESRCH))
        }
        // The process is running.
        Ok(()) => Ok(true),
    }
}

fn _main(args: Args) -> Result<(u8, Option<RawFd>), Box<dyn Error>> {
    let parent_tx = match args.mode {
        _ if args.debug => None,
        Mode::Acsint => None,
        Mode::Speakup => Some(start_daemon()?),
    };
    if let Some(tx) = parent_tx {
        if is_running(Path::new(&args.pid_path))? {
            println!("Espeakup is already running!");
            return Ok((1, Some(tx)));
        };

        let devnull = fcntl::open("/dev/null", OFlag::O_RDWR, stat::Mode::empty())?;
        unistd::dup2(devnull, libc::STDIN_FILENO)?;
        unistd::dup2(devnull, libc::STDOUT_FILENO)?;
        unistd::dup2(devnull, libc::STDERR_FILENO)?;
        if devnull > 2 {
            unistd::close(devnull)?;
        }
    };

    let run = Arc::new(AtomicBool::new(true));
    let run2 = Arc::clone(&run);
    let run3 = Arc::clone(&run);
    let (tx, rx) = mpsc::channel();
    let lock_condition = Arc::new((Mutex::new(false), Condvar::new()));
    let lock_condition2 = Arc::clone(&lock_condition);

    let softsynth_thread =
        thread::spawn(move || softsynth::thread(args.mode, run2, tx, lock_condition));
    let espeak_thread =
        thread::spawn(move || espeak::thread(args.voice, run3, rx, lock_condition2));

    // Let parent process terminate.
    if let Some(tx) = parent_tx {
        _ = unistd::write(tx, b"0")?;
    };

    // Let the main thread listen for signals.
    match Signals::new([libc::SIGINT, libc::SIGTERM]) {
        Ok(mut signals) => {
            for signal in signals.forever() {
                match signal {
                    libc::SIGINT | libc::SIGTERM => break,
                    _ => println!("espeakup caught signal {signal}"),
                }
            }
        }
        Err(error) => println!("signals {error}"),
    };

    run.store(false, Ordering::Relaxed);

    softsynth_thread
        .join()
        .expect("Couldn't join softsynth thread.");
    espeak_thread.join().expect("Couldn't join espeak thread.");

    Ok((0, None))
}

fn main() -> Result<(), Box<dyn Error>> {
    let args = Args::parse();
    unsafe {
        MODE = args.mode;
    }
    let result = _main(args.clone());
    if args.debug || matches!(args.mode, Mode::Acsint) {
        return Ok(());
    }
    match result {
        Ok((1, _)) => (),
        _ => unistd::unlink(Path::new(&args.pid_path))?,
    }
    match result {
        Ok((i, Some(tx))) => _ = unistd::write(tx, format!("{i}").as_bytes())?,
        Err(error) => return Err(error),
        _ => panic!(),
    }
    Ok(())
}
